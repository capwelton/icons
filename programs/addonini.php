;<?php/*

[general]
name="icons"
version="1.0.0"
encoding="UTF-8"
description="Provides constants used by icons libraries"
description.fr="Fourni les constantes utilisées dans les librairies d'icones"
delete=1
ov_version="8.6.100"
php_version="5.1.0"
addon_access_control="0"
mysql_character_set_database="latin1,utf8"
author="Robin Bailleux"
;*/?>